package com.immfly.backend.controllers.constants;

public final class ApiRoutes {
	
    public static final String API_ROOT = "/v1";
    
    public static final String FLIGHTS_BY_TAILNUMBER_CONTROLLER_PATH = "/flight-information-tail/{tail-number}";
    
    public static final String FLIGHTS_CONTROLLER_PATH = "/flight-information/{tail-number}/{flight-number}";
    
    private ApiRoutes() { }
}
