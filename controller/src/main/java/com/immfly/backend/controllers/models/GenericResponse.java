package com.immfly.backend.controllers.models;

import java.io.Serializable;

public class GenericResponse<T> implements Serializable
{
    private static final long serialVersionUID = 2602677976079525065L;
    private String status;
    private String code;
    private String message;
    private T data;
    
    public GenericResponse() {
    }
    
    public GenericResponse(final String status, final String code, final String message) {
        this.status = status;
        this.code = code;
        this.message = message;
    }
    
    public GenericResponse(final String status, final String code, final String message, final T data) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.data = data;
    }
    
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(final String status) {
        this.status = status;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public void setCode(final String code) {
        this.code = code;
    }
    
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(final String message) {
        this.message = message;
    }
    
    public T getData() {
        return this.data;
    }
    
    public void setData(final T data) {
        this.data = data;
    }
    
    @Override
    public String toString() {
        return "GenericResponse [status=" + this.status + ", code=" + this.code + ", message=" + this.message + ", data=" + this.data + "]";
    }
}
