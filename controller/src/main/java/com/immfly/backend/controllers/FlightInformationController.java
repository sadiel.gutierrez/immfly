package com.immfly.backend.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.immfly.backend.controllers.builders.GenericResponseBuilder;
import com.immfly.backend.controllers.constants.ApiRoutes;
import com.immfly.backend.controllers.models.GenericResponse;
import com.immfly.backend.domain.dtos.FlightInfoDto;
import com.immfly.backend.domain.service.interfaces.FlightInformationService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(ApiRoutes.API_ROOT)
public class FlightInformationController extends BaseController {

	private FlightInformationService flightInformationService;

	public FlightInformationController(FlightInformationService flightInformationService){
        this.flightInformationService = flightInformationService;
    }
	
	@ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get Flights Information")
	@GetMapping(value = ApiRoutes.FLIGHTS_CONTROLLER_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericResponse<List<FlightInfoDto>> getFlights(
    		@PathVariable(value = "tail-number") String tailNumber, 
    		@PathVariable(value = "flight-number") String flightNumber) {
		return new GenericResponseBuilder<List<FlightInfoDto>>(flightInformationService.getFlights(tailNumber, flightNumber)).build();
    }
	
	@ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get Flights Information By TailNumber")
	@GetMapping(value = ApiRoutes.FLIGHTS_BY_TAILNUMBER_CONTROLLER_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public GenericResponse<List<FlightInfoDto>> getFlightsByTailNumber(@PathVariable(value = "tail-number") String tailNumber) {
		return new GenericResponseBuilder<List<FlightInfoDto>>(flightInformationService.getFlightsByTailNumber(tailNumber)).build();
    }
}
