package com.immfly.backend.controllers;

import org.springframework.http.HttpStatus;

import com.immfly.backend.controllers.constants.CommonConstants;
import com.immfly.backend.controllers.models.GenericResponse;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = {
        @ApiResponse(code = 200, response = GenericResponse.class, message = "Success"),
        @ApiResponse(code = 202, response = GenericResponse.class, message = "Accepted"),
        @ApiResponse(code = 500, response = GenericResponse.class, message = "Internal Server Error"),
        @ApiResponse(code = 401, response = GenericResponse.class, message = "Unauthorized"),
        @ApiResponse(code = 400, response = GenericResponse.class, message = "Bad Request")})
public class BaseController {
	
    protected <T> GenericResponse<T> ok(T data) {
        return new GenericResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.SUCCESS, data);
    }

    protected <T> GenericResponse<T> created(T data) {
        return new GenericResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.CREATED), CommonConstants.SUCCESS, data);
    }

    protected <T> GenericResponse<T> notFound(T data) {
        return new GenericResponse<>(CommonConstants.KO, String.valueOf(HttpStatus.NOT_FOUND), CommonConstants.NOT_FOUND_MESSAGE, data);
    }

    protected <T> GenericResponse<T> accepted() {
        return new GenericResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.ACCEPTED), CommonConstants.SUCCESS, null);
    }

    protected <T> GenericResponse<T> noContent() {
        return new GenericResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.NO_CONTENT), CommonConstants.SUCCESS, null);
    }
}