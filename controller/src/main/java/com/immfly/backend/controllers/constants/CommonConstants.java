package com.immfly.backend.controllers.constants;

public final class CommonConstants {
	
    public static final String SUCCESS = "Success";
    public static final String OK = "OK";
    public static final String KO = "KO";
   
    public static final String NOT_FOUND_MESSAGE = "Item not found";
  
    private CommonConstants() {
    }
}
