package com.immfly.backend.controllers.utils;

import java.util.ArrayList;
import java.util.List;

import com.immfly.backend.domain.dtos.FlightInfoDto;
import com.immfly.backend.domain.dtos.StationDto;

public class TestData {

    public FlightInfoDto getFlightInfoDto(){
    	
    	FlightInfoDto flightInfoDto = new FlightInfoDto();
    	
    	flightInfoDto.setAirline("IBB");
    	flightInfoDto.setIata("NT");
    	flightInfoDto.setFlightNumber("653");
    	flightInfoDto.setPlate("EC-MYT");
    	flightInfoDto.setType("Form_Airline");
    	flightInfoDto.setCodeshare("IBE123");
    	flightInfoDto.setBlocked(false);
    	flightInfoDto.setDiverted(false);
    	flightInfoDto.setCancelled(false);

        StationDto origin = new StationDto();
        origin.setCode("GCXO");
        origin.setCity("Tenerife");
        origin.setAirportName("Tenerife North (Los Rodeos)");

        StationDto destination = new StationDto();
        destination.setCode("GCGM");
        destination.setCity("La Gomera");
        destination.setAirportName("La Gomera");

        flightInfoDto.setOrigin(origin);
        flightInfoDto.setDestination(destination);

        return flightInfoDto;
    }

    public List<FlightInfoDto> getFlightInfoDtos(){
        List<FlightInfoDto> flightInfoDtos = new ArrayList<>();
        flightInfoDtos.add(getFlightInfoDto());
        return flightInfoDtos;
    }
}
