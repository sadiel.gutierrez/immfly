package com.immfly.backend.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.immfly.backend.controllers.models.GenericResponse;
import com.immfly.backend.controllers.utils.TestData;
import com.immfly.backend.domain.dtos.FlightInfoDto;
import com.immfly.backend.domain.service.interfaces.FlightInformationService;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FlightInformationControllerTest {

    @Mock
    private FlightInformationService flightInformationServiceMock;

    private FlightInformationController flightInformationControllerSut;
    
    private TestData testData;

    @BeforeAll
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        testData = new TestData();
        flightInformationControllerSut = new FlightInformationController(flightInformationServiceMock);
    }

    @Test
    public void When_getFlights_Then_Return_List() {

        List<FlightInfoDto> flightInfoDtos = testData.getFlightInfoDtos();
        
        Mockito.when(flightInformationServiceMock.getFlights(Matchers.anyString(), Matchers.anyString())).thenReturn(flightInfoDtos);

        GenericResponse<List<FlightInfoDto>> response = flightInformationControllerSut.getFlights("tailNumber", "flightNumber");

        assertTrue(response.getStatus().equals("OK"));
        assertEquals(flightInfoDtos, response.getData());
    }
    
    @Test
    public void When_getFlightsByTailNumber_Then_Return_List() {

        List<FlightInfoDto> flightInfoDtos = testData.getFlightInfoDtos();
        
        Mockito.when(flightInformationServiceMock.getFlightsByTailNumber(Matchers.anyString())).thenReturn(flightInfoDtos);

        GenericResponse<List<FlightInfoDto>> response = flightInformationControllerSut.getFlightsByTailNumber("tailNumber");

        assertTrue(response.getStatus().equals("OK"));
        assertEquals(flightInfoDtos, response.getData());
    }

    
}
