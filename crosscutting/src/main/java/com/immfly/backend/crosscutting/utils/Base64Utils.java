package com.immfly.backend.crosscutting.utils;

import java.nio.charset.Charset;
import java.util.Base64;

public class Base64Utils {	
	
    public String encodeBase64String(String value) {
		return new String(Base64.getDecoder().decode(value), Charset.forName("UTF-8"));
	}

	public String decodeBase64String(String value) {
		return Base64.getEncoder().encodeToString(value.getBytes());
	}	

}
