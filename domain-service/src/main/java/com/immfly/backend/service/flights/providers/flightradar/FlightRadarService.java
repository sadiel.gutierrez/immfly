package com.immfly.backend.service.flights.providers.flightradar;

public interface FlightRadarService {

	boolean executeFlightRadarFlightsImportProcess();
}
