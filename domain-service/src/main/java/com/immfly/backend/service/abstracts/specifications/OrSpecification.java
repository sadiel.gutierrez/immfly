package com.immfly.backend.service.abstracts.specifications;

import java.util.HashSet;
import java.util.Set;

public class OrSpecification<T> extends AbstractSpecification<T> {

	private Set<Specification<T>> specificationSet = new HashSet<Specification<T>>();
	
	public OrSpecification(Specification<T> specOne, Specification<T> specTwo) {
		specificationSet.add(specOne);
		specificationSet.add(specTwo);
	}
	
	public boolean isSatisfiedBy(T t) {
		for(Specification<T> spec : specificationSet ) {
			if(spec.isSatisfiedBy(t) ) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public AbstractSpecification<T> or(Specification<T> spec) {
		specificationSet.add(spec);
		return this;
	}

}
