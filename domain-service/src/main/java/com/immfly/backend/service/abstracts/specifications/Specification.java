package com.immfly.backend.service.abstracts.specifications;

public interface Specification<T> {
	public boolean isSatisfiedBy(T t);
}
