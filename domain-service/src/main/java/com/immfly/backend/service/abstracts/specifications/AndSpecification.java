package com.immfly.backend.service.abstracts.specifications;

import java.util.HashSet;
import java.util.Set;

public class AndSpecification<T> extends AbstractSpecification<T> {

	private Set<Specification<T>> specificationSet = new HashSet<Specification<T>>();
	
	public AndSpecification(Specification<T> specOne, Specification<T> specTwo) {
		specificationSet.add(specOne);
		specificationSet.add(specTwo);
	}
	
	public boolean isSatisfiedBy(T incommingSpec) {
		for( Specification<T> spec : specificationSet ) {
			if( !spec.isSatisfiedBy(incommingSpec) ) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public AbstractSpecification<T> and(Specification<T> spec) {
		specificationSet.add(spec);
		return this;
	}

}
