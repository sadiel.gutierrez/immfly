package com.immfly.backend.service.abstracts.specifications;

public class NotSpecification<T> extends AbstractSpecification<T> {

	private Specification<T> currentSpecification;
	
	public NotSpecification(Specification<T> spec) {
		this.currentSpecification = spec;
	}
	
	@Override
	public boolean isSatisfiedBy(T spec) {
		return !currentSpecification.isSatisfiedBy(spec);
	}

}
