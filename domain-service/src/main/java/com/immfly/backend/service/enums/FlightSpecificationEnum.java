package com.immfly.backend.service.enums;

public enum FlightSpecificationEnum {
	BY_FLIGHTNUMBER,
	BY_TAILNUMBER
}
