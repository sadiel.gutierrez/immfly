package com.immfly.backend.service.flights.specifications;

import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.service.abstracts.specifications.AbstractSpecification;

public class FlightNumberSpecification extends AbstractSpecification<FlightInfo> {

	private String flightNumber;	
	
	@Override
	public boolean isSatisfiedBy(FlightInfo flightInfo) {
		return flightInfo.getFlightNumber().equalsIgnoreCase(flightNumber);
	}
	
	public FlightNumberSpecification with(String flightNumber) {
		this.flightNumber = flightNumber;
		return this;
	}

}
