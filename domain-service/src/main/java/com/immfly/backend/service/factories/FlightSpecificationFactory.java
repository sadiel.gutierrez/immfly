package com.immfly.backend.service.factories;

import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.service.abstracts.specifications.AbstractSpecification;
import com.immfly.backend.service.enums.FlightSpecificationEnum;

public interface FlightSpecificationFactory {

	AbstractSpecification<FlightInfo> getFlightSpecification(FlightSpecificationEnum flightSpecificationEnum);
}
