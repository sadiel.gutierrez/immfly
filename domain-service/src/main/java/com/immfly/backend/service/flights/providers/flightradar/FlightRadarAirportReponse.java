package com.immfly.backend.service.flights.providers.flightradar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FlightRadarAirportReponse {
      
    private String code;
	
    private String city;
    
    @JsonProperty("alternate_ident")
    private String alternateIdentification;
    
    @JsonProperty("airport_name")
    private String airportName;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAlternateIdentification() {
		return alternateIdentification;
	}

	public void setAlternateIdentification(String alternateIdentification) {
		this.alternateIdentification = alternateIdentification;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}
}
