package com.immfly.backend.service.mappers;

import org.mapstruct.Mapper;

import com.immfly.backend.domain.entities.Station;
import com.immfly.backend.service.flights.providers.flightradar.FlightRadarAirportReponse;

@Mapper
public interface FlightRadarAirportMapper {

	Station stationFromFlightRadarAirportResponse(FlightRadarAirportReponse flightRadarAirportReponse);
}
