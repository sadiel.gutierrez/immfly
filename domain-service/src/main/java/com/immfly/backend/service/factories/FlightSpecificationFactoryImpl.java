package com.immfly.backend.service.factories;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Service;

import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.service.abstracts.specifications.AbstractSpecification;
import com.immfly.backend.service.enums.FlightSpecificationEnum;
import com.immfly.backend.service.flights.specifications.FlightNumberSpecification;
import com.immfly.backend.service.flights.specifications.TailNumberSpecification;

@Service
public class FlightSpecificationFactoryImpl implements FlightSpecificationFactory {

	@Override
	public AbstractSpecification<FlightInfo> getFlightSpecification(FlightSpecificationEnum flightSpecificationEnum) {
	
		switch (flightSpecificationEnum) {
		
		case BY_FLIGHTNUMBER:
			return new FlightNumberSpecification();
			
		case BY_TAILNUMBER:
			return new TailNumberSpecification();

		default:
			throw new NotImplementedException("Flight Specification factory not implemented. {}", flightSpecificationEnum.name());
		}		
	}
	
}
