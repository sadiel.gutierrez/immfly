package com.immfly.backend.service.abstracts.specifications;

public abstract class AbstractSpecification<T> implements Specification<T> {
	
	public abstract boolean isSatisfiedBy(T incommingSpec);
	
	public AbstractSpecification<T> or(Specification<T> spec) {
		return new OrSpecification<T>(this, spec);
	}

	public AbstractSpecification<T> and(Specification<T> spec) {
		return new AndSpecification<T>(this, spec);
	}

	public AbstractSpecification<T> not() {
		return new NotSpecification<T>(this);
	}

}
