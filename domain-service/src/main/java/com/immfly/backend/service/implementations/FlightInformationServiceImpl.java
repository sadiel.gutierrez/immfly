package com.immfly.backend.service.implementations;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.immfly.backend.domain.dtos.FlightInfoDto;
import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.domain.repository.interfaces.FlightInformationRepository;
import com.immfly.backend.domain.service.interfaces.FlightInformationService;
import com.immfly.backend.service.cache.constants.CacheConstants;
import com.immfly.backend.service.cache.providers.redis.FlightRedisCacheManager;
import com.immfly.backend.service.enums.FlightSpecificationEnum;
import com.immfly.backend.service.factories.FlightSpecificationFactory;
import com.immfly.backend.service.flights.specifications.FlightNumberSpecification;
import com.immfly.backend.service.flights.specifications.TailNumberSpecification;
import com.immfly.backend.service.mappers.FlightInfoMapper;

@Service
@Transactional
public class FlightInformationServiceImpl implements FlightInformationService {
	
	private static final Logger logger = LoggerFactory.getLogger(FlightInformationServiceImpl.class);

	private FlightInfoMapper flightInfoMapper;
	
	private FlightInformationRepository flightInformationRepository;
	
	private FlightSpecificationFactory flightSpecificationFactory;
	
	private FlightRedisCacheManager flightRedisCacheManager;
	
	@Autowired
	public FlightInformationServiceImpl(
			FlightInfoMapper flightInfoMapper,
			FlightInformationRepository flightInformationRepository,
			FlightSpecificationFactory flightSpecificationFactory,
			FlightRedisCacheManager flightRedisCacheManager) {
		this.flightInfoMapper = flightInfoMapper;
		this.flightInformationRepository = flightInformationRepository;
		this.flightSpecificationFactory = flightSpecificationFactory;
		this.flightRedisCacheManager = flightRedisCacheManager;
	}
	
	@Override
	public List<FlightInfoDto> getFlights(String tailNumber, String flightNumber) {
		
		if(StringUtils.isEmpty(tailNumber) || StringUtils.isEmpty(flightNumber)) {
			logger.warn("getFlights - tailNumber or flightNumber are null or empty");
			return Collections.emptyList();
		}
		
		List<FlightInfo> flights = flightRedisCacheManager.getAll(CacheConstants.FLIGHTS_CACHE_KEY);
		
		if(flights == null || flights.isEmpty()) {
			flights = flightInformationRepository.getAllFlights();
			flightRedisCacheManager.addOrUpdateItem(CacheConstants.FLIGHTS_CACHE_KEY, flights);
		}
		
		FlightNumberSpecification flightNumberSpecification = 
				(FlightNumberSpecification) flightSpecificationFactory.getFlightSpecification(
						FlightSpecificationEnum.BY_FLIGHTNUMBER);
		
		TailNumberSpecification tailtNumberSpecification = 
				(TailNumberSpecification) flightSpecificationFactory.getFlightSpecification(
						FlightSpecificationEnum.BY_TAILNUMBER);
		
		return flightInfoMapper.flightInfoToDtos(
				flights.stream()
				.filter(flight -> 
							(flightNumberSpecification.with(flightNumber)
						.and(tailtNumberSpecification.with(tailNumber))
					.isSatisfiedBy(flight)))
				.collect(Collectors.toList()));
	}	

	@Override
	public List<FlightInfoDto> getFlightsByTailNumber(String tailNumber) {
		
		if(StringUtils.isEmpty(tailNumber)) {
			logger.warn("getFlightsByTailNumber - tailNumber is empty");
			return Collections.emptyList();
		}
		
		List<FlightInfo> flights = flightRedisCacheManager.getAll(CacheConstants.FLIGHTS_CACHE_KEY);
		
		if(flights == null || flights.isEmpty()) {
			flights = flightInformationRepository.getAllFlights();
			flightRedisCacheManager.addOrUpdateItem(CacheConstants.FLIGHTS_CACHE_KEY, flights);
		}
		
		TailNumberSpecification tailtNumberSpecification = 
				(TailNumberSpecification) flightSpecificationFactory.getFlightSpecification(
						FlightSpecificationEnum.BY_TAILNUMBER);
		
		return flightInfoMapper.flightInfoToDtos(
				flights.stream().filter(flight -> tailtNumberSpecification.with(tailNumber).isSatisfiedBy(flight))
				.collect(Collectors.toList()));
	}

}
