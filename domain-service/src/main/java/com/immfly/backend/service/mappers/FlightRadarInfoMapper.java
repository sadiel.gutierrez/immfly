package com.immfly.backend.service.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.service.flights.providers.flightradar.FlightRadarResponse;

@Mapper(uses = { FlightRadarAirportMapper.class })
public interface FlightRadarInfoMapper {

	FlightInfo flightInfoFromFlightRadarResponse(FlightRadarResponse flightRadarResponse);
	
	List<FlightInfo> flightInfoFromFlightRadarResponseList(List<FlightRadarResponse> FlightRadarResponseList);
}
