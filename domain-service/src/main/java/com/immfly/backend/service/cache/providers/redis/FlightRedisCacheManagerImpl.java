package com.immfly.backend.service.cache.providers.redis;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.immfly.backend.domain.entities.FlightInfo;

@Service
@Scope("singleton")
public class FlightRedisCacheManagerImpl implements FlightRedisCacheManager {
			
    private HashOperations<String, String, List<FlightInfo>> redisCacheHashOperationService;
    
    @Autowired
    public FlightRedisCacheManagerImpl(@Qualifier("redisFlightInfoTemplate") RedisTemplate<String, List<FlightInfo>> redisCacheTemplateService) { 
    	redisCacheHashOperationService = redisCacheTemplateService.opsForHash();
    }
    
    public List<FlightInfo> getAll(String key) {
    	return StreamSupport
    	  .stream(redisCacheHashOperationService.entries(key).values().spliterator(), false)
    	  .flatMap(List::stream)
    	  .collect(Collectors.toList());
    }
    
    public void addOrUpdateItem(String key, List<FlightInfo> value) {
    	redisCacheHashOperationService.put(key, key, value);
    }
    
    public void delete(String key) {
    	redisCacheHashOperationService.delete(key);
    }
    
}
