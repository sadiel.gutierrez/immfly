package com.immfly.backend.service.flights.providers.flightradar;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FlightRadarResponse {
    
    private String airline;
        
    private String iata;
        
    private String flightNumber;
        
    private String plate;
    
    private String type;
        
    private String codeshare;

    private Boolean blocked;
    
    private Boolean diverted;
    
    private Boolean cancelled;
    
    private FlightRadarAirportReponse origin;
    
    private FlightRadarAirportReponse destination;

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	@JsonProperty("airline_iata")
	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	@JsonProperty("flightnumber")
	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	@JsonProperty("tailnumber")
	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("codeshares")
	public String getCodeshare() {
		return codeshare;
	}

	public void setCodeshare(String codeshare) {
		this.codeshare = codeshare;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public Boolean getDiverted() {
		return diverted;
	}

	public void setDiverted(Boolean diverted) {
		this.diverted = diverted;
	}

	public Boolean getCancelled() {
		return cancelled;
	}

	public void setCancelled(Boolean cancelled) {
		this.cancelled = cancelled;
	}

	public FlightRadarAirportReponse getOrigin() {
		return origin;
	}

	public void setOrigin(FlightRadarAirportReponse origin) {
		this.origin = origin;
	}

	public FlightRadarAirportReponse getDestination() {
		return destination;
	}

	public void setDestination(FlightRadarAirportReponse destination) {
		this.destination = destination;
	}
}
