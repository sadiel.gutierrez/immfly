package com.immfly.backend.service.flights.specifications;

import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.service.abstracts.specifications.AbstractSpecification;

public class TailNumberSpecification extends AbstractSpecification<FlightInfo> {

	private String tailNumber;
	
	@Override
	public boolean isSatisfiedBy(FlightInfo flightInfo) {
		return flightInfo.getPlate().equalsIgnoreCase(tailNumber);
	}
	
	public TailNumberSpecification with(String tailNumber) {
		this.tailNumber = tailNumber;
		return this;
	}
}
