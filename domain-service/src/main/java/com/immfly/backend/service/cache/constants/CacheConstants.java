package com.immfly.backend.service.cache.constants;

public final class CacheConstants {

	public static final String FLIGHTS_CACHE_KEY = "Flights";
	
	private CacheConstants () { }
}
