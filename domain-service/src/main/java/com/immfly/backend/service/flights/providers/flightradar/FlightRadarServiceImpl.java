package com.immfly.backend.service.flights.providers.flightradar;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.immfly.backend.domain.repository.interfaces.FlightInformationRepository;
import com.immfly.backend.service.mappers.FlightRadarInfoMapper;

@Service
@Transactional
public class FlightRadarServiceImpl implements FlightRadarService {
	
	private static final Logger logger = LoggerFactory.getLogger(FlightRadarServiceImpl.class);
	
	private FlightInformationRepository flightInformationRepository;
	
	private FlightRadarInfoMapper flightRadarInfoMapper;
	
	private ObjectMapper objectMapper;
	
	@Autowired
	public FlightRadarServiceImpl(
			FlightInformationRepository flightInformationRepository,
			FlightRadarInfoMapper flightRadarInfoMapper,
			Optional<ObjectMapper> objectMapper) {
		this.flightInformationRepository = flightInformationRepository;
		this.flightRadarInfoMapper = flightRadarInfoMapper;
		this.objectMapper = !objectMapper.isPresent() ? new ObjectMapper() : objectMapper.get();
	}

	@Override
	@Scheduled(cron = "0 0/5 * * * ?")
	public boolean executeFlightRadarFlightsImportProcess() {
		
		logger.info("executeFlightRadarFlightsData() - START");
		
		try {
			List<FlightRadarResponse> flightsRadarResponseList = objectMapper.readValue(
					ResourceUtils.getFile("classpath:flights-from-provider.json"), 
							new TypeReference<List<FlightRadarResponse>>(){});
			
			flightInformationRepository.saveAll(flightRadarInfoMapper.flightInfoFromFlightRadarResponseList(flightsRadarResponseList));
			
			logger.info("executeFlightRadarFlightsData() - {} records saved in database", flightsRadarResponseList.size());
			
			return true;
			
		} catch (IOException e) {
			logger.error("executeFlightRadarFlightsData() thrown exception trying to import flights from Flight Radar Flights Provider Service. ", e);
			return false;
		} finally {
			logger.info("executeFlightRadarFlightsData() - END");
		}	
		
	}
}
