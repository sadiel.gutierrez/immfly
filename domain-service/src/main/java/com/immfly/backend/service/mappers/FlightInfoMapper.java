package com.immfly.backend.service.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import com.immfly.backend.domain.dtos.FlightInfoDto;
import com.immfly.backend.domain.entities.FlightInfo;

@Mapper
public interface FlightInfoMapper {

	FlightInfoDto flightInfoToDto(FlightInfo flightInfo);
	FlightInfo flightInfoFromDto(FlightInfoDto flightInfoDto);
	
	List<FlightInfoDto> flightInfoToDtos(List<FlightInfo> flightInfoList);
	List<FlightInfo> flightInfoFromDtos(List<FlightInfoDto> flightInfoDtoList);
}
