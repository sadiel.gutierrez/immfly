package com.immfly.backend.service.cache.providers.redis;

import java.util.List;

import com.immfly.backend.domain.entities.FlightInfo;

public interface FlightRedisCacheManager {

	public List<FlightInfo> getAll(String key);
    
    public void addOrUpdateItem(String key, List<FlightInfo> value);
    
    public void delete(String key);
}
