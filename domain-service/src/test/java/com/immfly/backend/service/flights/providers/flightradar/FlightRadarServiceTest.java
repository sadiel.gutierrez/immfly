package com.immfly.backend.service.flights.providers.flightradar;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.domain.repository.interfaces.FlightInformationRepository;
import com.immfly.backend.service.mappers.FlightRadarInfoMapper;
import com.immfly.backend.service.utils.TestData;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FlightRadarServiceTest {

	@Mock
	private FlightInformationRepository flightInformationRepositoryMock;
	
	@Mock
	private FlightRadarInfoMapper flightRadarInfoMapperMock;
	
	@Mock
	private ObjectMapper objectMapperMock;
	
	private FlightRadarService flightRadarServiceSut = new FlightRadarServiceImpl(null, null, Optional.empty());
	
	private TestData testData;
	
	@BeforeAll
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        testData = new TestData();
        flightRadarServiceSut = new FlightRadarServiceImpl(
        		flightInformationRepositoryMock,
        		flightRadarInfoMapperMock,
        		Optional.of(objectMapperMock));
    }

    @Test
    public void When_executeFlightRadarFlightsImportProcess_Then_Success() throws IOException {
        
        List<FlightInfo> flightInfos = testData.getFlightInfos();
        
        List<FlightRadarResponse> flightRadarResponses = testData.getFlightRadarResponses();
        
        Mockito.when(objectMapperMock.readValue(
        		Matchers.any(java.io.File.class), 
        		Matchers.any(com.fasterxml.jackson.core.type.TypeReference.class)))
        .thenReturn(flightRadarResponses);
        
        Mockito.when(flightRadarInfoMapperMock.flightInfoFromFlightRadarResponseList(Matchers.any())).thenReturn(flightInfos);
        
        Mockito.doNothing().when(flightInformationRepositoryMock).saveAll(Matchers.anyList());

        Boolean executionResult = flightRadarServiceSut.executeFlightRadarFlightsImportProcess();
        
        assertTrue(executionResult);
    }
    
    @Test
    public void When_executeFlightRadarFlightsImportProcess_And_ExceptionThrown_Then_Failed() throws IOException {
        
        Mockito.when(objectMapperMock.readValue(
        		Matchers.any(java.io.File.class), 
        		Matchers.any(com.fasterxml.jackson.core.type.TypeReference.class)))
        .thenThrow(new IOException("error trying to deserialize json file"));

        Boolean executionResult = flightRadarServiceSut.executeFlightRadarFlightsImportProcess();
        
        assertTrue(!executionResult);
    }
}
