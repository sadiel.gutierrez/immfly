package com.immfly.backend.service.implementations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.immfly.backend.domain.dtos.FlightInfoDto;
import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.domain.repository.interfaces.FlightInformationRepository;
import com.immfly.backend.domain.service.interfaces.FlightInformationService;
import com.immfly.backend.service.cache.providers.redis.FlightRedisCacheManager;
import com.immfly.backend.service.enums.FlightSpecificationEnum;
import com.immfly.backend.service.factories.FlightSpecificationFactory;
import com.immfly.backend.service.flights.specifications.FlightNumberSpecification;
import com.immfly.backend.service.flights.specifications.TailNumberSpecification;
import com.immfly.backend.service.mappers.FlightInfoMapper;
import com.immfly.backend.service.utils.TestData;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FlightInformationServiceTest {

	@Mock
	private FlightInfoMapper flightInfoMapperMock;
	
	@Mock
	private FlightInformationRepository flightInformationRepositoryMock;
	
	@Mock
	private FlightSpecificationFactory flightSpecificationFactoryMock;
	
	@Mock
	private FlightRedisCacheManager flightRedisCacheManagerMock;

    private FlightInformationService FlightInformationServiceSut;
    
    private TestData testData;
	    
	@BeforeAll
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        testData = new TestData();
        FlightInformationServiceSut = new FlightInformationServiceImpl(
        		flightInfoMapperMock,
    			flightInformationRepositoryMock,
    			flightSpecificationFactoryMock,
    			flightRedisCacheManagerMock);
    }

    @Test
    public void When_getFlights_Then_Return_ListFromCache() {

        List<FlightInfoDto> flightInfoDtos = testData.getFlightInfoDtos();
        
        List<FlightInfo> flightInfos = testData.getFlightInfos();
        
        Mockito.when(flightRedisCacheManagerMock.getAll(Matchers.anyString())).thenReturn(flightInfos);
        
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_FLIGHTNUMBER)).thenReturn(new FlightNumberSpecification());
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_TAILNUMBER)).thenReturn(new TailNumberSpecification());
        
        Mockito.when(flightInfoMapperMock.flightInfoToDtos(Matchers.any())).thenReturn(flightInfoDtos);

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlights("tailNumber", "flightNumber");
        
        assertEquals(flights.size(), flightInfos.size());
    }
    
    @Test
    public void When_getFlightsByTailNumber_Then_Return_ListFromCache() {

        List<FlightInfoDto> flightInfoDtos = testData.getFlightInfoDtos();
        
        List<FlightInfo> flightInfos = testData.getFlightInfos();
        
        Mockito.when(flightRedisCacheManagerMock.getAll(Matchers.anyString())).thenReturn(flightInfos);
        
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_TAILNUMBER)).thenReturn(new TailNumberSpecification());
        
        Mockito.when(flightInfoMapperMock.flightInfoToDtos(Matchers.any())).thenReturn(flightInfoDtos);

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlightsByTailNumber("tailNumber");
        
        assertEquals(flights.size(), flightInfos.size());
    }
    
    @Test
    public void When_getFlights_And_CacheResultIsNull_Then_Return_ListFromRepository() {

        List<FlightInfoDto> flightInfoDtos = testData.getFlightInfoDtos();
        
        List<FlightInfo> flightInfos = testData.getFlightInfos();
        
        Mockito.when(flightRedisCacheManagerMock.getAll(Matchers.anyString())).thenReturn(null);
        
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_FLIGHTNUMBER)).thenReturn(new FlightNumberSpecification());
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_TAILNUMBER)).thenReturn(new TailNumberSpecification());
        
        Mockito.when(flightInfoMapperMock.flightInfoToDtos(Matchers.any())).thenReturn(flightInfoDtos);
        Mockito.when(flightInformationRepositoryMock.getAllFlights()).thenReturn(flightInfos);

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlights("tailNumber", "flightNumber");
        
        assertEquals(flights.size(), flightInfos.size());
    }
    
    @Test
    public void When_getFlightsByTailNumber_And_CacheResultIsNull_Then_Return_ListFromRepository() {

        List<FlightInfoDto> flightInfoDtos = testData.getFlightInfoDtos();
        
        List<FlightInfo> flightInfos = testData.getFlightInfos();
        
        Mockito.when(flightRedisCacheManagerMock.getAll(Matchers.anyString())).thenReturn(null);
        
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_TAILNUMBER)).thenReturn(new TailNumberSpecification());
        
        Mockito.when(flightInfoMapperMock.flightInfoToDtos(Matchers.any())).thenReturn(flightInfoDtos);
        Mockito.when(flightInformationRepositoryMock.getAllFlights()).thenReturn(flightInfos);

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlightsByTailNumber("tailNumber");
        
        assertEquals(flights.size(), flightInfos.size());
    }
    
    @Test
    public void When_getFlights_And_CacheResultIsEmpty_Then_Return_ListFromRepository() {

        List<FlightInfoDto> flightInfoDtos = testData.getFlightInfoDtos();
        
        List<FlightInfo> flightInfos = testData.getFlightInfos();
        
        Mockito.when(flightRedisCacheManagerMock.getAll(Matchers.anyString())).thenReturn(Collections.emptyList());
        
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_FLIGHTNUMBER)).thenReturn(new FlightNumberSpecification());
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_TAILNUMBER)).thenReturn(new TailNumberSpecification());
        
        Mockito.when(flightInfoMapperMock.flightInfoToDtos(Matchers.any())).thenReturn(flightInfoDtos);
        Mockito.when(flightInformationRepositoryMock.getAllFlights()).thenReturn(flightInfos);

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlights("tailNumber", "flightNumber");
        
        assertEquals(flights.size(), flightInfos.size());
    }
    
    @Test
    public void When_getFlightsByTailNumber_And_CacheResultIsEmpty_Then_Return_ListFromRepository() {

        List<FlightInfoDto> flightInfoDtos = testData.getFlightInfoDtos();
        
        List<FlightInfo> flightInfos = testData.getFlightInfos();
        
        Mockito.when(flightRedisCacheManagerMock.getAll(Matchers.anyString())).thenReturn(Collections.emptyList());
        
        Mockito.when(flightSpecificationFactoryMock.getFlightSpecification(FlightSpecificationEnum.BY_TAILNUMBER)).thenReturn(new TailNumberSpecification());
        
        Mockito.when(flightInfoMapperMock.flightInfoToDtos(Matchers.any())).thenReturn(flightInfoDtos);
        Mockito.when(flightInformationRepositoryMock.getAllFlights()).thenReturn(flightInfos);

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlightsByTailNumber("tailNumber");
        
        assertEquals(flights.size(), flightInfos.size());
    }
    
    @Test
    public void When_getFlights_And_FlightNumberIsEmpty_Then_Return_EmptyList() {

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlights("tailNumber", "");
        
        assertTrue(flights.isEmpty());
    }
    
    @Test
    public void When_getFlights_And_TailNumberIsEmpty_Then_Return_EmptyList() {

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlights("", "flightNumber");
        
        assertTrue(flights.isEmpty());
    }
    
    @Test
    public void When_getFlightsByTailNumber_And_TailNumberIsEmpty_Then_Return_EmptyList() {

        List<FlightInfoDto> flights = FlightInformationServiceSut.getFlightsByTailNumber("");
        
        assertTrue(flights.isEmpty());
    }
}
