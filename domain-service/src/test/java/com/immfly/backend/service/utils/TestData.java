package com.immfly.backend.service.utils;

import java.util.ArrayList;
import java.util.List;

import com.immfly.backend.domain.dtos.FlightInfoDto;
import com.immfly.backend.domain.dtos.StationDto;
import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.domain.entities.Station;
import com.immfly.backend.service.flights.providers.flightradar.FlightRadarAirportReponse;
import com.immfly.backend.service.flights.providers.flightradar.FlightRadarResponse;

public class TestData {

    public FlightInfoDto getFlightInfoDto(){
    	
    	FlightInfoDto flightInfoDto = new FlightInfoDto();
    	
    	flightInfoDto.setAirline("IBB");
    	flightInfoDto.setIata("NT");
    	flightInfoDto.setFlightNumber("653");
    	flightInfoDto.setPlate("EC-MYT");
    	flightInfoDto.setType("Form_Airline");
    	flightInfoDto.setCodeshare("IBE123");
    	flightInfoDto.setBlocked(false);
    	flightInfoDto.setDiverted(false);
    	flightInfoDto.setCancelled(false);

        StationDto origin = new StationDto();
        origin.setCode("GCXO");
        origin.setCity("Tenerife");
        origin.setAirportName("Tenerife North (Los Rodeos)");

        StationDto destination = new StationDto();
        destination.setCode("GCGM");
        destination.setCity("La Gomera");
        destination.setAirportName("La Gomera");

        flightInfoDto.setOrigin(origin);
        flightInfoDto.setDestination(destination);

        return flightInfoDto;
    }

    public List<FlightInfoDto> getFlightInfoDtos(){
        List<FlightInfoDto> flightInfoDtos = new ArrayList<>();
        flightInfoDtos.add(getFlightInfoDto());
        return flightInfoDtos;
    }
    
    public FlightInfo getFlightInfo(){
    	
    	FlightInfo flightInfo = new FlightInfo();
    	
    	flightInfo.setAirline("IBB");
    	flightInfo.setIata("NT");
    	flightInfo.setFlightNumber("653");
    	flightInfo.setPlate("EC-MYT");
    	flightInfo.setType("Form_Airline");
    	flightInfo.setCodeshare("IBE123");
    	flightInfo.setBlocked(false);
    	flightInfo.setDiverted(false);
    	flightInfo.setCancelled(false);

        Station origin = new Station();
        origin.setCode("GCXO");
        origin.setCity("Tenerife");
        origin.setAirportName("Tenerife North (Los Rodeos)");

        Station destination = new Station();
        destination.setCode("GCGM");
        destination.setCity("La Gomera");
        destination.setAirportName("La Gomera");

        flightInfo.setOrigin(origin);
        flightInfo.setDestination(destination);

        return flightInfo;
    }

    public List<FlightInfo> getFlightInfos(){
        List<FlightInfo> flightInfos = new ArrayList<>();
        flightInfos.add(getFlightInfo());
        return flightInfos;
    }
    
    public FlightRadarResponse getFlightRadarResponse(){
    	
    	FlightRadarResponse flightRadarResponse = new FlightRadarResponse();
    	
    	flightRadarResponse.setAirline("IBB");
    	flightRadarResponse.setIata("NT");
    	flightRadarResponse.setFlightNumber("653");
    	flightRadarResponse.setPlate("EC-MYT");
    	flightRadarResponse.setType("Form_Airline");
    	flightRadarResponse.setCodeshare("IBE123");
    	flightRadarResponse.setBlocked(false);
    	flightRadarResponse.setDiverted(false);
    	flightRadarResponse.setCancelled(false);

    	FlightRadarAirportReponse origin = new FlightRadarAirportReponse();
        origin.setCode("GCXO");
        origin.setCity("Tenerife");
        origin.setAirportName("Tenerife North (Los Rodeos)");

        FlightRadarAirportReponse destination = new FlightRadarAirportReponse();
        destination.setCode("GCGM");
        destination.setCity("La Gomera");
        destination.setAirportName("La Gomera");

        flightRadarResponse.setOrigin(origin);
        flightRadarResponse.setDestination(destination);

        return flightRadarResponse;
    }

    public List<FlightRadarResponse> getFlightRadarResponses(){
        List<FlightRadarResponse> flightRadarResponses = new ArrayList<>();
        flightRadarResponses.add(getFlightRadarResponse());
        return flightRadarResponses;
    }
}
