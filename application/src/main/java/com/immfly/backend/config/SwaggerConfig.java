package com.immfly.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket newsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("immfly-backend").apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("com.immfly.backend.controllers")).build();
	}

	private ApiInfo apiInfo() {

		return new ApiInfoBuilder().title("immfly-backend API for Spring Microservice Architecture")
				.description("immfly-backend API for Spring Microservice Architecture").version("1.0").build();
	}
}
