package com.immfly.backend.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import com.immfly.backend.domain.entities.FlightInfo;

@Configuration
public class RedisConfig {

	@Bean
	@Qualifier("redisFlightInfoTemplate")
	public RedisTemplate<String, List<FlightInfo>> redisFlightInfoTemplate(RedisConnectionFactory connectionFactory) {
	    RedisTemplate<String, List<FlightInfo>> redisTemplate = new RedisTemplate<>();
	    redisTemplate.setConnectionFactory(connectionFactory);
	    redisTemplate.afterPropertiesSet();
	    return redisTemplate;
	}
}
