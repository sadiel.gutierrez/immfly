package com.immfly.backend.repository.implementation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.immfly.backend.domain.entities.FlightInfo;
import com.immfly.backend.domain.repository.interfaces.FlightInformationRepository;

@Repository
public class FlightInformationRepositoryImpl implements FlightInformationRepository {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	public FlightInformationRepositoryImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<FlightInfo> getAllFlights() {
		
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		
	    CriteriaQuery<FlightInfo> criteriaQuery = criteriaBuilder.createQuery(FlightInfo.class);
	    
	    Root<FlightInfo> rootEntry = criteriaQuery.from(FlightInfo.class);
	    
	    return entityManager.createQuery(criteriaQuery.select(rootEntry)).getResultList();
	}

	@Override
	public void saveAll(List<FlightInfo> flights) {
		flights.forEach(flight -> entityManager.persist(flight));	
	}		
    
}
