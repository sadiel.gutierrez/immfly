FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY application /tmp/application/
COPY crosscutting /tmp/crosscutting/
COPY controller /tmp/controller/
COPY domain-model /tmp/domain-model/
COPY domain-service /tmp/domain-service/
COPY infrastructure /tmp/infrastructure/
WORKDIR /tmp/
RUN mvn clean install -Pdocker

FROM openjdk:8-jdk-alpine
COPY --from=MAVEN_TOOL_CHAIN /tmp/application/target/immfly-backend.jar app.jar

RUN sh -c 'touch /app.jar'

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]