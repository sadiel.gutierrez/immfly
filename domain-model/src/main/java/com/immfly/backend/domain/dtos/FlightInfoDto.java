package com.immfly.backend.domain.dtos;

import com.immfly.backend.domain.entities.Station;

public class FlightInfoDto {

	private String id;
    
    private String airline;
    
    private String iata;
    
    private String flightNumber;
    
    private String plate;
    
    private String type;
    
    private String codeshare;

    private Boolean blocked;
    
    private Boolean diverted;
    
    private Boolean cancelled;

    private StationDto origin;
    
    private StationDto destination;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getIata() {
		return iata;
	}

	public void setIata(String iata) {
		this.iata = iata;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCodeshare() {
		return codeshare;
	}

	public void setCodeshare(String codeshare) {
		this.codeshare = codeshare;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public Boolean getDiverted() {
		return diverted;
	}

	public void setDiverted(Boolean diverted) {
		this.diverted = diverted;
	}

	public Boolean getCancelled() {
		return cancelled;
	}

	public void setCancelled(Boolean cancelled) {
		this.cancelled = cancelled;
	}

	public StationDto getOrigin() {
		return origin;
	}

	public void setOrigin(StationDto origin) {
		this.origin = origin;
	}

	public StationDto getDestination() {
		return destination;
	}

	public void setDestination(StationDto destination) {
		this.destination = destination;
	}
}
