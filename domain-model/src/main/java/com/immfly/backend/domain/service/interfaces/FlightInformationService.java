package com.immfly.backend.domain.service.interfaces;

import java.util.List;

import com.immfly.backend.domain.dtos.FlightInfoDto;

public interface FlightInformationService {

    List<FlightInfoDto> getFlights(String tailNumber, String flightNumber);

    List<FlightInfoDto> getFlightsByTailNumber(String tailNumber);
}
