package com.immfly.backend.domain.repository.interfaces;

import java.util.List;

import com.immfly.backend.domain.entities.FlightInfo;

public interface FlightInformationRepository {

	List<FlightInfo> getAllFlights();
	
	void saveAll(List<FlightInfo> flights);
}
